from django.apps import AppConfig


class BabybytesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'babybytes'
